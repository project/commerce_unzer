COMMERCE UNZER
--------------

Commerce Unzer integrates the provider Unzer with Drupal the Commerce payment and
checkout system.

Please install the module using the composer workflow:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules

DRUPAL VERSION
--------------

Drupal 10.1 and up


PHP VERSION
-----------------------------

PHP 8.1 and up, just like Drupal 10.1.


PHP CONFIGURATION
-----------------

Unzer will throw exceptions due to rounding errors for some amounts
to be paid if this setting is not set to -1 in your PHP configuration:
serialize_precision = -1

See https://docs.unzer.com/server-side-integration/php-sdk-integration/php-installation/#php-configuration and
https://www.php.net/manual/en/ini.core.php#ini.serialize-precision


CONFIGURING PAYMENT METHOD
--------------------------

1. Get your public and private key from Unzer service.

For test credentials, please review the following resources:
- https://docs.unzer.com/reference/test-data/
- https://dev.unzer.com/sandbox-environment/

2. Insert your API keys at the Commerce payment configuration page
   admin/commerce/config/payment-gateways
   Remember to test the functionality with the test keys before going live!


HISTORY
-------

Unzer used to be called Heidelpay but has changed its name.
For Heidelpay there is a module:
https://www.drupal.org/project/commerce_heidelpay

The original heidelpay API library has been deprecated by Unzer
and has been replaced with a new SDK (https://github.com/unzerdev/php-sdk).
This module strives to use the current API under the new provider name.

The branch 1.0.x of commerce_unzer is based on the branch 8.x-1.x of commerce_heidelpay.


CREDITS
-------

Commerce Unzer integration has been written by:

- Christian Spitzlay [(cspitzlay)](https://www.drupal.org/u/cspitzlay)
- Markus Kalkbrenner [(mkalkbrenner)](https://www.drupal.org/u/mkalkbrenner)
- Tim Seifert [(tseifert)](https://www.drupal.org/u/tseifert)

Commerce Heidelpay integration that this module is based on has been written by:

- Pedro Cambra [(pcambra)](https://www.drupal.org/u/pcambra)
- Manuel Egío [(facine)](https://www.drupal.org/u/facine)


SPONSORS
--------

Commerce Unzer:
- [Jarltech Europe GmbH](https://www.drupal.org/jarltech-europe-gmbh)

Commerce Heidelpay:
- [Doctors Without Borders Germany](https://www.aerzte-ohne-grenzen.de)


CONTACT
-------
Developed and maintained by [Cambrico](http://cambrico.net) and [Ymbra](https://ymbra.com).

Get in touch with us for customizations and consultancy:
http://cambrico.net/contact
