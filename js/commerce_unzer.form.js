/**
 * @file
 * Javascript to generate Unzer form.
 */

(function ($, Drupal, once, drupalSettings) {

  'use strict';

  /**
   * Attaches the commerceUnzerForm behavior.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.commerceUnzerForm = {
    cardNumber: null,
    cardExpiry: null,
    cardCvc: null,

    attach: function (context, settings) {
      var self = this;
      if (!drupalSettings.commerceUnzer || !drupalSettings.commerceUnzer.publicKey) {
        return;
      }
      $(once('unzer-processed','.unzerUI', context)).each(function () {
        var $form = $(this).closest('form');
        try {
          var unzerInstance = new unzer(drupalSettings.commerceUnzer.publicKey);
        }
        catch (e) {
          $form.find('#error-holder').html(Drupal.theme('commerceUnzerError', e.message));
          $form.find(':input.button--primary').prop('disabled', true);
          $(this).find('.form-item').hide();
          return;
        }

        // Creating a credit card instance.
        var Card = unzerInstance.Card();
        var errorHolder = $('#error-holder');

        self.cardNumber = Card.create('number', {
          containerId: 'card-element-id-number',
          onlyIframe: false
        });
        Card.create('expiry', {
          containerId: 'card-element-id-expiry',
          onlyIframe: false
        });
        Card.create('cvc', {
          containerId: 'card-element-id-cvc',
          onlyIframe: false
        });
        if ($("#card-element-id-holder").length){
          Card.create('holder', {
            containerId: 'card-element-id-holder',
            onlyIframe: false
          });
        }
        $form.on('submit', function (e) {
          if ($('#unzer-resource-id', $form).val().length > 0) {
            return true;
          }
          Card.createResource()
            .then(function (result) {
              $('#unzer-resource-id', $form).val(result.id);
              $form.find(':input.button--primary').click();
            })
            .catch(function (error) {
              // Errors are handled inline by the unzer library.
            });
          e.preventDefault();
        });
      });
    },

    detach: function (context, settings, trigger) {
      if (trigger !== 'unload') {
        return;
      }
      var $form = $('.unzerUI', context).closest('form');
      if ($form.length === 0) {
        return;
      }
      $form.off('submit');
    }

  };

})(jQuery, Drupal, once, drupalSettings);
