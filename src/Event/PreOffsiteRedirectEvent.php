<?php

namespace Drupal\commerce_unzer\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Fired before redirection to offsite paypage at Unzer.
 */
class PreOffsiteRedirectEvent extends Event {

  /**
   * Constructs a new PreOffsiteRedirectEvent object.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order about to be paid for.
   * @param array<string,mixed> $paypageSettings
   *   The array of paypage settings.
   * @param string[] $excludedPaymentTypes
   *   The array of excluded payment types.
   */
  public function __construct(protected OrderInterface $order, protected array $paypageSettings, protected array $excludedPaymentTypes) {}

  /**
   * @return string[]
   */
  public function getExcludedPaymentTypes(): array {
    return $this->excludedPaymentTypes;
  }

  /**
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order about to be paid for.
   */
  public function getOrder(): OrderInterface {
    return $this->order;
  }

  /**
   * @return array<string,mixed>
   *   The array with the paypage settings.
   */
  public function getPaypageSettings(): array {
    return $this->paypageSettings;
  }

  /**
   * @param array<int,string> $excluded_payment_types
   *   The array of excluded payment types.
   */
  public function setExcludedPaymentTypes(array $excluded_payment_types): void {
    $this->excludedPaymentTypes = $excluded_payment_types;
  }

  /**
   * @param array<string,mixed> $paypage_settings
   *   The array of paypage settings.
   */
  public function setPaypageSettings(array $paypage_settings): void {
    $this->paypageSettings = $paypage_settings;
  }

}
