<?php

namespace Drupal\commerce_unzer\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Fired when the Unzer order ID is to be determined.
 */
class DetermineUnzerOrderIdEvent extends Event {

  /**
   * Constructs a new DetermineUnzerOrderIdEvent.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param string $unzerOrderId
   *   The order ID to set at Unzer when the transaction is prepared.
   */
  public function __construct(protected OrderInterface $order, protected string $unzerOrderId) {}

  public function getOrder(): OrderInterface {
    return $this->order;
  }

  public function getUnzerOrderId(): string {
    return $this->unzerOrderId;
  }

  public function setUnzerOrderId(string $unzer_order_id): void {
    $this->unzerOrderId = $unzer_order_id;
  }

}
