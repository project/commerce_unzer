<?php

namespace Drupal\commerce_unzer;

use Drupal\Core\Logger\LoggerChannelInterface;
use Psr\Log\LogLevel;
use UnzerSDK\Interfaces\DebugHandlerInterface;

class DebugHandler implements DebugHandlerInterface {

  /**
   * Constructs a new DebugHandler object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The Drupal logger for Unzer messages.
   */
  public function __construct(protected LoggerChannelInterface $logger) {}

  /**
   * {@inheritdoc}
   */
  public function log(string $message): void {
    $this->logger->log(LogLevel::DEBUG, $message);
  }

}
