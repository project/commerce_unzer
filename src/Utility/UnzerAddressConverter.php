<?php

namespace Drupal\commerce_unzer\Utility;

use Drupal\address\Plugin\Field\FieldType\AddressItem;

/**
 * Converts an address item to something the Unzer API will accept.
 *
 * Takes a Drupal Commerce profile's address item as used in the billing address
 * and prepares the data for use with the Unzer SDK.
 */
class UnzerAddressConverter {

  final public const ADDRESS_NAME = 'addressname';

  final public const CITY = 'city';

  final public const COMPANY = 'company';

  final public const COUNTRY = 'country';

  final public const FIRSTNAME = 'firstname';

  final public const LASTNAME = 'lastname';

  final public const STATE = 'state';

  final public const STREET = 'street';

  final public const ZIP = 'zip';

  /**
   * Converts an address item to something the Unzer API will accept.
   *
   * Takes an Address item from a Drupal Commerce profile to an array of
   * address data to be used in the Unzer SDK for the customer and billing
   * address objects.  Truncates fields as needed to satisfy Unzer size limits.
   *
   * @see https://docs.unzer.com/accept-payments/manage-payments/customer/
   *
   * @param \Drupal\address\Plugin\Field\FieldType\AddressItem $address
   *   The address item to take the data from.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   *
   * @return array<string,string>
   *   An array of address data to be used in the Unzer SDK.
   *   The used array keys are defined as public constants to avoid typos.
   */
  public static function convertAddressToUnzerData(AddressItem $address): array {
    $commerce_array = $address->toArray();

    // Customer profiles can be configured to not capture first and last name.
    // This is a case we already support but sometimes the values are not just
    // empty but missing -> initialize them here.
    $commerce_array += [
      'given_name' => '',
      'family_name' => '',
    ];

    // Prepare values for use in the customer object. Apply field-specific
    // length restrictions.
    $given_name = mb_substr(trim((string) $commerce_array['given_name']), 0, 40, 'UTF-8');
    $family_name = mb_substr(trim((string) $commerce_array['family_name']), 0, 40, 'UTF-8');
    $name_string = implode(' ', array_filter([$given_name, $family_name]));

    // Firstname and lastname are mandatory fields when creating an Unzer
    // customer. If the Drupal site really chooses to not capture them we
    // fall back to 'Unknown'.
    if (empty($given_name)) {
      $given_name = 'Unknown';
    }

    if (empty($family_name)) {
      $family_name = 'Unknown';
    }

    $organization = mb_substr(trim($commerce_array['organization'] ?? ''), 0, 40, 'UTF-8');

    // Prepare values for use in the billing address object. Apply
    // field-specific length restrictions.
    $street1 = trim($commerce_array['address_line1'] ?? '');
    $street2 = trim($commerce_array['address_line2'] ?? '');
    $street_string = implode(', ', array_filter([$street1, $street2]));
    $street_string = mb_substr($street_string, 0, 50, 'UTF-8');

    $administrative_area = mb_substr(trim($commerce_array['administrative_area'] ?? ''), 0, 8, 'UTF-8');
    $postal_code = mb_substr(trim($commerce_array['postal_code'] ?? ''), 0, 10, 'UTF-8');
    $locality = mb_substr(trim($commerce_array['locality'] ?? ''), 0, 30, 'UTF-8');
    $country_code = mb_substr(trim($commerce_array['country_code'] ?? ''), 0, 2, 'UTF-8');

    return [
      self::FIRSTNAME => $given_name,
      self::LASTNAME => $family_name,
      self::ADDRESS_NAME => $name_string,
      self::STREET => $street_string,
      self::ZIP => $postal_code,
      self::CITY => $locality,
      self::STATE => $administrative_area,
      self::COUNTRY => $country_code,
      self::COMPANY => $organization,
    ];
  }

}
