<?php

namespace Drupal\commerce_unzer\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_unzer\Utility\UnzerAddressConverter;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\DrupalKernel;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use UnzerSDK\Exceptions\UnzerApiException;
use UnzerSDK\Resources\Basket;
use UnzerSDK\Resources\CustomerFactory;
use UnzerSDK\Resources\EmbeddedResources\Address;
use UnzerSDK\Resources\EmbeddedResources\BasketItem;
use UnzerSDK\Resources\PaymentTypes\Card;
use UnzerSDK\Resources\TransactionTypes\Charge;
use UnzerSDK\Unzer;

/**
 * Provides the Unzer payment gateway (On Site).
 *
 * @CommercePaymentGateway(
 *     id="commerce_unzer_onsite",
 *     label="Unzer (On Site)",
 *     display_label="Pay with credit or debit card",
 *     forms={
 *         "add-payment-method": "Drupal\commerce_unzer\PluginForm\Unzer\PaymentMethodAddForm",
 *     },
 *     payment_method_types={"credit_card"},
 *     credit_card_types={
 *         "amex",
 *         "mastercard",
 *         "visa",
 *     },
 *     js_library="commerce_unzer/form",
 *     requires_billing_information=FALSE,
 *
 * )
 */
class UnzerOnSite extends OnsitePaymentGatewayBase implements UnzerOnSiteInterface {

  /**
   * Constructs a new UnzerOnsite object.
   *
   * @param array<string,mixed> $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel factory.
   * @param \Drupal\Core\DrupalKernel $drupalKernel
   *   The drupal kernel.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user account.
   */
  public function __construct(array $configuration, $plugin_id, mixed $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, protected LoggerChannelInterface $logger, protected DrupalKernel $drupalKernel, protected RequestStack $requestStack, protected AccountProxyInterface $currentUser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
  }

  /**
   * {@inheritdoc}
   *
   * @param array<string,mixed> $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array<string,mixed>
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private Key'),
      '#default_value' => $this->configuration['private_key'],
      '#size' => 60,
    ];
    $form['public_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public Key'),
      '#default_value' => $this->configuration['public_key'],
      '#size' => 60,
    ];
    $form['3ds'] = [
      '#type' => 'radios',
      '#title' => $this->t('3ds enabled'),
      '#default_value' => $this->configuration['3ds'],
      '#options' => [
        TRUE => $this->t('Enabled'),
        FALSE => $this->t('Disabled'),
      ],
      '#description' => $this->t('When enabled, the user will need to complete an additional 3ds validation.'),
    ];
    $form['account_holder'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Request account holder on checkout'),
      '#default_value' => $this->configuration['account_holder'],
      '#description' => $this->t('A field requesting the account holder will be displayed on checkout.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   * @param array<string,mixed> $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param $plugin_definition
   *   The plugin definition.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory')->get('commerce_unzer'),
      $container->get('kernel'),
      $container->get('request_stack'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE): void {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    \assert($payment_method instanceof PaymentMethodInterface);
    $this->assertPaymentMethod($payment_method);
    $order = $payment->getOrder();
    \assert($order instanceof OrderInterface);

    try {
      $unzer = new Unzer($this->getConfiguration()['private_key']);
      $unzer_payment_type = $unzer->fetchPaymentType($payment_method->getRemoteId());
      $payment_data = $order->getData('commerce_unzer');

      if (!isset($payment_data['payment_id'], $payment_data['transaction_id'])) {
        $checkout_url = Url::fromRoute('commerce_checkout.form', ['commerce_order' => $order->id()], ['absolute' => TRUE])->toString();

        // Add customer info from the billing profile, default to the user data.
        if ($billing_profile = $payment_method->getBillingProfile()) {
          /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $billing_address */
          // @phpstan-ignore-next-line
          $billing_address = $billing_profile->get('address')->first();
          $converted_address = UnzerAddressConverter::convertAddressToUnzerData($billing_address);

          $billing_address_unzer = new Address();

          if (!empty($converted_address[UnzerAddressConverter::STREET])) {
            $billing_address_unzer->setStreet($converted_address[UnzerAddressConverter::STREET]);
          }

          if (!empty($converted_address[UnzerAddressConverter::STATE])) {
            $billing_address_unzer->setState($converted_address[UnzerAddressConverter::STATE]);
          }

          if (!empty($converted_address[UnzerAddressConverter::ZIP])) {
            $billing_address_unzer->setZip($converted_address[UnzerAddressConverter::ZIP]);
          }

          if (!empty($converted_address[UnzerAddressConverter::CITY])) {
            $billing_address_unzer->setCity($converted_address[UnzerAddressConverter::CITY]);
          }

          if (!empty($converted_address[UnzerAddressConverter::COUNTRY])) {
            $billing_address_unzer->setCountry($converted_address[UnzerAddressConverter::COUNTRY]);
          }

          $customer = CustomerFactory::createCustomer($converted_address[UnzerAddressConverter::FIRSTNAME], $converted_address[UnzerAddressConverter::LASTNAME]);

          if (!empty($converted_address[UnzerAddressConverter::COMPANY])) {
            $customer->setCompany($converted_address[UnzerAddressConverter::COMPANY]);
          }
          $customer->setBillingAddress($billing_address_unzer);
        }
        else {
          $name = $this->currentUser->getDisplayName();
          $customer = CustomerFactory::createCustomer($name, $name);
        }
        $customer->setEmail($this->currentUser->getEmail());

        // Add basket/cart info to the order.
        $basket_items = [];

        foreach ($order->getItems() as $order_item) {
          if ($order_item->getUnitPrice()->isZero()) {
            // Unzer validation will reject prices of zero. Skip creating a basket item.
            continue;
          }
          // @TODO Add a more sophisticated breakdown of items, such as discounts, VAT, etc.
          $basket_item = new BasketItem();
          $basket_item->setTitle($order_item->label())
            ->setAmountPerUnitGross((float) $order_item->getTotalPrice()->getNumber())
            ->setQuantity((int) $order_item->getQuantity());
          $basket_items[] = $basket_item;
        }
        $basket = new Basket();
        $basket->setOrderId($order->id())
          ->setCurrencyCode($order->getTotalPrice()->getCurrencyCode())
          ->setTotalValueGross((float) $order->getTotalPrice()->getNumber())
          ->setBasketItems($basket_items);

        /** @var \UnzerSDK\Resources\TransactionTypes\Charge $charge */
        $charge = (new Charge((float) $order->getTotalPrice()->getNumber(), $order->getTotalPrice()->getCurrencyCode(), $checkout_url))
          ->setOrderId($order->id());
        $charge->setCard3ds((bool) $this->getConfiguration()['3ds']);
        $unzer_transaction = $unzer->performCharge($charge, $unzer_payment_type->getId(), $customer, NULL, $basket);

        // Set the transaction and payment id in case of 3ds, so the process can
        // return to where it was and finish the transaction.
        $order->setData('commerce_unzer', [
          'payment_id' => $unzer_transaction->getPaymentId(),
          'transaction_id' => $unzer_transaction->getId(),
        ]);
        $order->save();
      }
      else {
        $transaction_payment = $unzer->fetchPayment($payment_data['payment_id']);
        $unzer_transaction = $transaction_payment->getCharge($payment_data['transaction_id']);
      }
      $should_redirect = !empty($unzer_transaction->getRedirectUrl());

      // Redirect to the 3ds page or to success depending on the state of the
      // transaction.
      switch (TRUE) {
        case !$should_redirect && $unzer_transaction->isSuccess():
          // Save the payment.
          $payment->setRemoteId($payment_data['payment_id']);
          $payment->save();

          break;

        case (!$should_redirect && $unzer_transaction->isPending()) || $unzer_transaction->isError():
          $this->logger->warning($unzer_transaction->getMessage()->getCode());

          throw new PaymentGatewayException('There was an error processing the payment.');

        case $should_redirect && $unzer_transaction->isPending():
          // Redirect in case of 3ds.
          $response = new RedirectResponse($unzer_transaction->getRedirectUrl());
          // Save session and ensure that the kernel terminate subscribers can
          // run.
          $request = $this->requestStack->getCurrentRequest();
          $request->getSession()->save();
          $response->prepare($request);
          $this->drupalKernel->terminate($request, $response);
          $response->send();

          exit();
      }
    }
    catch (UnzerApiException $e) {
      // @TODO: Review the different unzer errors & throw contextual errors.
      $this->logger->error($e->getCode() . ': ' . $e->getMessage());

      throw new PaymentGatewayException('There was an error processing the payment.');
    }
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   * @param array<string,mixed> $payment_details
   *   The payment details.
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details): void {
    $required_keys = [
      // The expected keys are payment gateway specific and usually match
      // the PaymentMethodAddForm form elements. They are expected to be valid.
      'unzer_resource_id',
    ];

    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new PaymentGatewayException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    try {
      $unzer = new Unzer($this->getConfiguration()['private_key']);
      /** @var \UnzerSDK\Resources\PaymentTypes\Card $payment_type */
      $payment_type = $unzer->fetchPaymentType($payment_details['unzer_resource_id']);
      \assert($payment_type instanceof Card);

      // @phpstan-ignore-next-line
      $payment_method->card_type = $this->cardBrandEquivalences($payment_type->getBrand());
      // @phpstan-ignore-next-line
      $payment_method->card_number = $payment_type->getNumber();
      [$month, $year] = explode('/', $payment_type->getExpiryDate());
      // @phpstan-ignore-next-line
      $payment_method->card_exp_month = $month;
      // @phpstan-ignore-next-line
      $payment_method->card_exp_year = $year;
      $payment_method->setRemoteId($payment_details['unzer_resource_id']);
      $expires = CreditCard::calculateExpirationTimestamp($month, $year);
      $payment_method->setExpiresTime($expires);
      $payment_method->save();
    }
    catch (\Exception $e) {
      // @TODO: Review the different unzer errors & throw contextual errors.
      $this->logger->error($e->getCode() . ': ' . $e->getMessage());

      throw new PaymentGatewayException('There was an error processing the payment.');
    }
  }

  /**
   * {@inheritdoc}
   *
   * @return array<string,mixed>
   */
  public function defaultConfiguration(): array {
    return [
      'private_key' => '',
      'public_key' => '',
      '3ds' => TRUE,
      'account_holder' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method to delete.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method): void {
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function getPublicKey(): string {
    return $this->configuration['public_key'];
  }

  /**
   * {@inheritdoc}
   *
   * @param array<string,mixed> $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration['private_key'] = $values['private_key'];
      $this->configuration['public_key'] = $values['public_key'];
      $this->configuration['3ds'] = $values['3ds'];
      $this->configuration['account_holder'] = $values['account_holder'];
    }
  }

  /**
   * Translates the Unzer card type to what Commerce understands.
   *
   * @param string $brand
   *   The brand of the card on Unzer.
   *
   * @return string
   *   The commerce card type.
   */
  protected function cardBrandEquivalences(string $brand): string {
    $types = [
      'AMEX' => 'amex',
      'MASTER' => 'mastercard',
      'VISA' => 'visa',
      'VISAELECTRON' => 'visa',
      'MAESTRO' => 'mastercard',
    ];

    return $types[$brand];
  }

}
