<?php

namespace Drupal\commerce_unzer\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;

/**
 * Provides the interface for the Unzer On Site payment gateway.
 */
interface UnzerOnSiteInterface extends OnsitePaymentGatewayInterface {

  /**
   * Get the Unzer public key set for the payment gateway.
   *
   * @return string
   *   The Unzer public key.
   */
  public function getPublicKey(): string;

  // @TODO SupportsAuthorizationsInterface, SupportsRefundsInterface?

}
