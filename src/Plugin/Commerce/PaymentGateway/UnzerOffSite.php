<?php

namespace Drupal\commerce_unzer\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\HasPaymentInstructionsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_unzer\DebugHandler;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use UnzerSDK\Constants\PaymentState;
use UnzerSDK\Exceptions\UnzerApiException;
use UnzerSDK\Resources\TransactionTypes\AbstractTransactionType;
use UnzerSDK\Unzer;

/**
 * Provides the Unzer payment gateway (Off Site).
 *
 * @CommercePaymentGateway(
 *     id="commerce_unzer_offsite",
 *     label="Unzer (Off Site)",
 *     display_label="Pay with credit or debit card",
 *     forms={
 *         "offsite-payment": "Drupal\commerce_unzer\PluginForm\OffsiteRedirect\UnzerForm",
 *     },
 *     payment_method_types={"credit_card"},
 *     credit_card_types={
 *         "amex",
 *         "dinersclub",
 *         "discover",
 *         "jcb",
 *         "mastercard",
 *         "visa",
 *     },
 *     requires_billing_information=FALSE,
 * )
 */
class UnzerOffSite extends OffsitePaymentGatewayBase implements HasPaymentInstructionsInterface {

  /**
   * The logger.
   */
  protected LoggerChannelInterface $logger;

  /**
   * Constructs a new UnzerOffSite object.
   *
   * @param array<string,mixed> $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The locking layer instance.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   The current route match service.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   */
  public function __construct(array $configuration, $plugin_id, mixed $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, LoggerChannelFactoryInterface $logger_channel_factory, protected LockBackendInterface $lock, protected CurrentRouteMatch $currentRouteMatch, protected EventDispatcherInterface $eventDispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->logger = $logger_channel_factory->get('commerce_unzer');
  }

  /**
   * {@inheritdoc}
   *
   * @param array<string,mixed> $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array<string,mixed>
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private Key'),
      '#default_value' => $this->configuration['private_key'],
      '#size' => 60,
    ];
    $form['public_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public Key'),
      '#default_value' => $this->configuration['public_key'],
      '#size' => 60,
    ];
    $form['enable_3ds'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable 3D-secure (3ds) for credit card payments'),
      '#description' => $this->t('Your contract with Unzer needs to support this for this feature to work.'),
      '#default_value' => $this->configuration['enable_3ds'],
    ];
    $form['instructions'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Payment instructions'),
      '#description' => $this->t('Shown at the end of checkout, after the customer has placed their order.'),
      '#default_value' => $this->configuration['instructions']['value'],
      '#format' => $this->configuration['instructions']['format'],
    ];
    $form['debug_logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug Logging'),
      '#description' => $this->t('Whether to write debugging output to the Drupal logger channel.'),
      '#default_value' => $this->configuration['debug_logging'],
    ];

    $form['ept_details'] = [
      '#type' => 'details',
      '#title' => $this->t('Excluded payment types'),
      '#description' => $this->t('Unzer payment types that should <em>not</em> be available when using this payment gateway even if your Unzer account configuration allowed them.'),
      '#open' => FALSE,
    ];
    $form['ept_details']['excluded_payment_types'] = [
      '#type' => 'checkboxes',
      '#default_value' => $this->configuration['excluded_payment_types'],
      '#options' => $this->getUnzerPaymentTypes(),
      '#multiple' => TRUE,
    ];
    $form['paypage_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Paypage settings'),
      '#description' => $this->t('Optional settings to be applied to the Unzer paypage.'),
      '#open' => FALSE,
    ];

    foreach ($this->getUnzerOptionalPaypageSettings() as $setting_name => $setting_data) {
      $form['paypage_settings'][$setting_name] = [
        '#type' => 'textfield',
        '#title' => $this->t($setting_data['title']),
        '#description' => $this->t($setting_data['description']),
        '#default_value' => $this->configuration['paypage_settings'][$setting_name],
        '#size' => 60,
      ];
    }

    // Make it clear that this option does not change how Unzer will treat the
    // transactions.
    $form['mode']['#description'] = $this->t('NB: This will mark the payments in Drupal but will <em>not</em> change how Unzer will treat the transactions. If you need to make test transactions you will need test keys from Unzer and set those as private and public keys.');

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   *
   * @return array<string,mixed>
   *   A render array with payment instructions from the configuration.
   */
  public function buildPaymentInstructions(PaymentInterface $payment): array {
    $configuration = $this->getConfiguration();

    $instructions = [];

    if (!empty($configuration['instructions']['value'])) {
      $instructions = [
        '#type' => 'processed_text',
        '#text' => $configuration['instructions']['value'],
        '#format' => $configuration['instructions']['format'],
      ];
    }

    return $instructions;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   * @param array<string,mixed> $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The ID of the plugin.
   * @param $plugin_definition
   *   The plugin definition.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory'),
      $container->get('lock'),
      $container->get('current_route_match'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @return array<string,mixed>
   */
  public function defaultConfiguration(): array {
    $defaults = [
      'private_key' => '',
      'public_key' => '',
      'enable_3ds' => TRUE,
      'instructions' => [
        'value' => '',
        'format' => 'plain_text',
      ],
      'debug_logging' => FALSE,
      'excluded_payment_types' => [],
    ];

    foreach ($this->getUnzerOptionalPaypageSettings() as $setting_name => $setting_data) {
      $defaults['paypage_settings'][$setting_name] = '';
    }

    return $defaults + parent::defaultConfiguration();
  }

  /**
   * Gets the mapping of optional attributes for the Unzer paypage window.
   *
   * @return array<string, array<string, string>> The mapping of optional settings for the Unzer paypage
   *   (Drupal plugin setting name => Unzer method name)
   */
  public function getUnzerOptionalPaypageSettings(): array {
    return [
      'logo_image' => [
        'title' => 'The URL to your logo',
        'description' => 'Make sure the image file is accessible by Unzer or any transaction will error out.',
        'method' => 'setLogoImage',
      ],
      'full_page_image' => [
        'title' => 'The URL to the background image for the payment window',
        'description' => 'Make sure the imagefile is accessible by Unzer or any transaction will error out.',
        'method' => 'setFullPageImage',
      ],
      'shop_name' => [
        'title' => 'The name of your shop',
        'description' => '',
        'method' => 'setShopName',
      ],
      'shop_description' => [
        'title' => 'The description of your shop',
        'description' => '',
        'method' => 'setShopDescription',
      ],
      'tagline' => [
        'title' => 'Your tag line',
        'description' => 'Displayed as subtitle in the payment window.',
        'method' => 'setTagline',
      ],
      'terms_and_conditions_url' => [
        'title' => 'The URL of your terms and conditions page',
        'description' => '',
        // The Unzer SDK method really has "condition" in the singular form.
        'method' => 'setTermsAndConditionUrl',
      ],
      'privacy_policy_url' => [
        'title' => 'The URL of your privacy policy page',
        'description' => '',
        'method' => 'setPrivacyPolicyUrl',
      ],
      'imprint_url' => [
        'title' => 'The URL of your imprint page',
        'description' => '',
        'method' => 'setImprintUrl',
      ],
      'help_url' => [
        'title' => 'The URL of your help page',
        'description' => '',
        'method' => 'setHelpUrl',
      ],
      'contact_url' => [
        'title' => 'The URL of your contact page',
        'description' => '',
        'method' => 'setContactUrl',
      ],
    ];
  }

  /**
   * Called if the user hits the "Back to merchant" button.
   *
   * Will send the user back to the previous step of the checkout flow.
   *
   * This method is called manually, since Unzer does not have a cancel URL.
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  public function onCancel(OrderInterface $order, Request $request): void {
    parent::onCancel($order, $request);

    // Take one step back.
    /** @var \Drupal\commerce_checkout\Entity\CheckoutFlowInterface $checkout_flow */
    // @phpstan-ignore-next-line
    $checkout_flow = $order->get('checkout_flow')->entity;
    $checkout_flow_plugin = $checkout_flow->getPlugin();
    // @phpstan-ignore-next-line
    $step_id = $order->get('checkout_step')->value;
    $previous_step_id = $checkout_flow_plugin->getPreviousStepId($step_id);
    $checkout_flow_plugin->redirectToStep($previous_step_id);
    // The above does not return. It throws a NeedsRedirectException.
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request): ?Response {
    // @TODO.
    return NULL;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  public function onReturn(OrderInterface $order, Request $request): void {
    $data = $order->getData('commerce_unzer');

    if (empty($data['payment_id'])) {
      throw new InvalidRequestException('An error has occurred trying to process the payment data, please contact us.');
    }
    $payment_id = $data['payment_id'];

    // Unzer does not seem to have a separate cancel URL but the user shows up
    // at the return URL.  So we need to find out whether the user has clicked
    // "Back to merchant".
    try {
      $unzer = new Unzer($this->getConfiguration()['private_key']);

      $unzer->setDebugMode($this->configuration['debug_logging'])
        ->setDebugHandler(new DebugHandler($this->logger));

      $unzer_payment = $unzer->fetchPayment($payment_id);

      // The state that is returned when clicking "Back to Merchant" is 6.
      // The associated constant is PaymentState::STATE_CREATE; see also the
      // mapping in PaymentState::mapStateCodeToName.
      if ($unzer_payment->getState() === PaymentState::STATE_CREATE) {
        $this->onCancel($order, $request);
        // The above step does not return but throws a NeedsRedirectException
        // that we need to let through.
      }
    }
    catch (UnzerApiException $uae) {
      throw new InvalidRequestException('An error has occurred trying to process the payment data, please contact us.', 0, $uae);
    }

    $this->processNotification($order, $payment_id);
  }

  /**
   * {@inheritdoc}
   *
   * @param array<string,mixed> $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration['private_key'] = $values['private_key'];
      $this->configuration['public_key'] = $values['public_key'];
      $this->configuration['enable_3ds'] = $values['enable_3ds'];
      $this->configuration['instructions'] = $values['instructions'];
      $this->configuration['debug_logging'] = $values['debug_logging'];
      // ept_details is just a wrapper, move values one level up in the config
      $this->configuration['excluded_payment_types'] = $values['ept_details']['excluded_payment_types'];
      // paypage_settings is a logical grouping of settings, keep extra level
      $this->configuration['paypage_settings'] = $values['paypage_settings'];
    }
  }

  /**
   * Creates an array of Unzer payment types keyed by their resource names.
   *
   * The class names are taken from the class definitions in
   * vendor/unzerdev/php-sdk/src/Resources/PaymentTypes.
   *
   * @return array<string,\Drupal\Core\StringTranslation\TranslatableMarkup> An array of payment types (resource name => translated name)
   */
  protected function getUnzerPaymentTypes(): array {
    $payment_types = [];
    $classes = [
      'Alipay' => 'Alipay',
      'Applepay' => 'Applepay',
      'Bancontact' => 'Bancontact',
      'Card' => 'Card',
      'EPS' => 'EPS',
      'Giropay' => 'Giropay',
      'Ideal' => 'Ideal',
      'InstallmentSecured' => 'Installment - Secured',
      'Invoice' => 'Invoice',
      'InvoiceSecured' => 'Invoice - Secured',
      'Paypage' => 'Paypage',
      'Paypal' => 'Paypal',
      'PIS' => 'PIS',
      'Prepayment' => 'Prepayment',
      'Przelewy24' => 'Przelewy 24',
      'SepaDirectDebit' => 'Sepa Direct Debit',
      'SepaDirectDebitSecured' => 'Sepa Direct Debit - Secured',
      'Sofort' => 'Sofort',
      'Wechatpay' => 'Wechatpay',
    ];

    foreach ($classes as $class => $label) {
      $full_class_name = 'UnzerSDK\\Resources\\PaymentTypes\\' . $class;
      $type_id = $full_class_name::getResourceName();
      $payment_types[$type_id] = $this->t($label, [], ['context' => 'Unzer payment type']);
    }

    return $payment_types;
  }

  /**
   * Processes the notification.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param string $payment_id
   *   The external payment id.
   *
   * @throws \Drupal\commerce_payment\Exception\InvalidRequestException
   */
  protected function processNotification(OrderInterface $order, string $payment_id): void {
    try {
      $unzer = new Unzer($this->getConfiguration()['private_key']);

      $unzer->setDebugMode($this->configuration['debug_logging'])->setDebugHandler(new DebugHandler($this->logger));

      $unzer_payment = $unzer->fetchPayment($payment_id);

      $unzer_payment_state = $unzer_payment->getStateName();

      if ($unzer_payment_state !== PaymentState::STATE_NAME_COMPLETED) {
        throw new InvalidRequestException('An error has occurred trying to process the payment data, please contact us.');
      }

      // Use the short ID of the initial transaction as remote ID because that
      // is the one displayed in Unzer insights. Fall back to the payment ID as
      // a last resort if we cannot get the short ID.
      $initial_transaction = $unzer_payment->getInitialTransaction();
      $remote_id = $payment_id;

      if ($initial_transaction instanceof AbstractTransactionType && !empty($initial_transaction->getShortId())) {
        $remote_id = $initial_transaction->getShortId();
      }

      $payment = $this->entityTypeManager->getStorage('commerce_payment')
        ->create([
          'state' => $unzer_payment_state,
          'amount' => $order->getTotalPrice(),
          'payment_gateway' => $this->parentEntity->id(),
          'order_id' => $order->id(),
          'remote_id' => $remote_id,
          'remote_state' => $unzer_payment_state,
          'authorized' => $this->time->getRequestTime(),
        ]);
      $payment->save();
    }
    catch (\Exception $e) {
      throw new InvalidRequestException('An error has occurred trying to process the payment data, please contact us.', 0, $e);
    }
  }

}
