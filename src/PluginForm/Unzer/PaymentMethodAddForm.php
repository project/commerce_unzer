<?php

namespace Drupal\commerce_unzer\PluginForm\Unzer;

use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormStateInterface;

/**
 * Unzer add-form class.
 */
class PaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * {@inheritdoc}
   *
   * @param array<string,mixed> $element
   *   The element to add data to.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array<string,mixed>
   */
  public function buildCreditCardForm(array $element, FormStateInterface $form_state): array {
    $element['#attributes']['class'][] = 'unzerUI';
    $element['#attributes']['class'][] = 'form';

    // Set our key to settings array.
    /** @var \Drupal\commerce_unzer\Plugin\Commerce\PaymentGateway\UnzerOnSiteInterface $plugin */
    $plugin = $this->plugin;

    $element['#attached']['drupalSettings']['commerceUnzer'] = [
      'publicKey' => $plugin->getPublicKey(),
    ];

    // Populated by the JS library.
    $element['unzer_resource_id'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'unzer-resource-id',
      ],
    ];

    $element['card_number'] = [
      '#type' => 'item',
      '#required' => TRUE,
      '#validated' => TRUE,
      '#markup' => '<div id="card-element-id-number" class="unzerInput"></div>',
    ];

    $element['expiration'] = [
      '#type' => 'item',
      '#required' => TRUE,
      '#validated' => TRUE,
      '#markup' => '<div id="card-element-id-expiry" class="unzerInput"></div>',
    ];

    $element['security_code'] = [
      '#type' => 'item',
      '#required' => TRUE,
      '#validated' => TRUE,
      '#markup' => '<div id="card-element-id-cvc" class="unzerInput"></div>',
    ];

    $element['account_holder'] = [
      '#type' => 'item',
      '#required' => TRUE,
      '#validated' => TRUE,
      '#markup' => '<div id="card-element-id-holder" class="unzerInput"></div>',
      '#access' => $plugin->getConfiguration()['account_holder'] ?? FALSE,
    ];

    // To display validation errors.
    $element['payment_errors'] = [
      '#type' => 'markup',
      '#markup' => '<div id="error-holder"></div>',
      '#weight' => -200,
    ];

    $cacheability = new CacheableMetadata();
    $cacheability->addCacheableDependency($this->entity);
    $cacheability->setCacheMaxAge(0);
    $cacheability->applyTo($element);

    return $element;
  }

  /**
   * {@inheritdoc}
   *
   * @param array<string,mixed> $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitCreditCardForm(array $element, FormStateInterface $form_state): void {
    if ($email = $form_state->getValue(['contact_information', 'email'])) {
      $email_parents = array_merge($element['#parents'], ['email']);
      $form_state->setValue($email_parents, $email);
    }
  }

  /**
   * {@inheritdoc}
   *
   * @param array<string,mixed> $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function validateCreditCardForm(array &$element, FormStateInterface $form_state): void {
    // The JS library performs its own validation.
  }

}
