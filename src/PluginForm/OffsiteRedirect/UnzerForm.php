<?php

namespace Drupal\commerce_unzer\PluginForm\OffsiteRedirect;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\commerce_unzer\DebugHandler;
use Drupal\commerce_unzer\Event\DetermineUnzerOrderIdEvent;
use Drupal\commerce_unzer\Event\PreOffsiteRedirectEvent;
use Drupal\commerce_unzer\Utility\UnzerAddressConverter;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use UnzerSDK\Constants\ApiResponseCodes;
use UnzerSDK\Constants\BasketItemTypes;
use UnzerSDK\Exceptions\UnzerApiException;
use UnzerSDK\Resources\Basket;
use UnzerSDK\Resources\CustomerFactory;
use UnzerSDK\Resources\EmbeddedResources\Address;
use UnzerSDK\Resources\EmbeddedResources\BasketItem;
use UnzerSDK\Resources\PaymentTypes\Paypage;
use UnzerSDK\Unzer;

/**
 * Provides the Unzer class for the payment form.
 */
class UnzerForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Constructs a new UnzerForm object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   */
  public function __construct(protected MessengerInterface $messenger, protected TimeInterface $time, protected EntityTypeManagerInterface $entityTypeManager, protected LoggerChannelInterface $logger, protected EventDispatcherInterface $eventDispatcher) {}

  /**
   * {@inheritdoc}
   *
   * @param array<string,mixed> $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   *
   * @return array<string,mixed>
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();
    /** @var \Drupal\commerce_unzer\Plugin\Commerce\PaymentGateway\UnzerOffSite $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    /** @var array<string,mixed> $gateway_settings */
    $gateway_settings = $payment_gateway_plugin->getConfiguration();
    $order_user = $order->getCustomer();

    try {
      $unzer = new Unzer($gateway_settings['private_key']);

      $unzer->setDebugMode($gateway_settings['debug_logging'])
        ->setDebugHandler(new DebugHandler($this->logger));

      if ($billing_profile = $order->getBillingProfile()) {
        // Billing address is available, use it.
        /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $billing_address */
        // @phpstan-ignore-next-line
        $billing_address = $billing_profile->get('address')->first();

        $converted_address = UnzerAddressConverter::convertAddressToUnzerData($billing_address);

        $billing_address_unzer = new Address();

        // The Unzer docs say: "Use this field if the name for billing is
        // different to the customer name". Since we currently get firstname and
        // lastname from Drupal Commerce's billing address they will be the
        // same, and we will not set it here.
        // if (!empty($converted_address[UnzerAddressConverter::ADDRESS_NAME])) {
        // $billing_address_unzer->setName($converted_address[UnzerAddressConverter::ADDRESS_NAME);
        // }
        if (!empty($converted_address[UnzerAddressConverter::STREET])) {
          $billing_address_unzer->setStreet($converted_address[UnzerAddressConverter::STREET]);
        }

        if (!empty($converted_address[UnzerAddressConverter::STATE])) {
          $billing_address_unzer->setState($converted_address[UnzerAddressConverter::STATE]);
        }

        if (!empty($converted_address[UnzerAddressConverter::ZIP])) {
          $billing_address_unzer->setZip($converted_address[UnzerAddressConverter::ZIP]);
        }

        if (!empty($converted_address[UnzerAddressConverter::CITY])) {
          $billing_address_unzer->setCity($converted_address[UnzerAddressConverter::CITY]);
        }

        if (!empty($converted_address[UnzerAddressConverter::COUNTRY])) {
          $billing_address_unzer->setCountry($converted_address[UnzerAddressConverter::COUNTRY]);
        }
      }
      else {
        // Fallback: Use the minimal user data we have.
        $converted_address[UnzerAddressConverter::FIRSTNAME] = '';
        $converted_address[UnzerAddressConverter::LASTNAME] = $order_user->getDisplayName();
      }

      // Create or retrieve an Unzer customer object
      if ($order_user->isAnonymous()) {
        // Always create a new Unzer customer, there is no way to recognize
        // recurring customers if they are anonymous.
        $customer = CustomerFactory::createCustomer($converted_address[UnzerAddressConverter::FIRSTNAME], $converted_address[UnzerAddressConverter::LASTNAME]);
        $unzer->createCustomer($customer);
      }
      else {
        // External customer IDs need to be unique by Unzer key pair.
        // We define "our" unzer customer ID to include the Drupal user ID so
        // this is satisfied for any payments made from a single Drupal instance.
        $unzer_customer_id = 'uid-' . $order_user->id();

        try {
          $customer = $unzer->fetchCustomerByExtCustomerId($unzer_customer_id);
          $customer->setFirstname($converted_address[UnzerAddressConverter::FIRSTNAME]);
          $customer->setLastname($converted_address[UnzerAddressConverter::LASTNAME]);

          if (!empty($converted_address[UnzerAddressConverter::COMPANY])) {
            $customer->setCompany($converted_address[UnzerAddressConverter::COMPANY]);
          }
        }
        catch (UnzerApiException $e) {
          // Although UnzerApiException inherits from Exception the return type
          // of getCode() is not int like the base class suggests.  The UnzerSDK
          // intercepts the constructor call and puts in their API's response
          // code that is returned as part of the JSON-encoded API response.
          // That response code is of type string, e.g. "API.410.100.100".
          // @phpstan-ignore-next-line
          if ($e->getCode() === ApiResponseCodes::API_ERROR_CUSTOMER_DOES_NOT_EXIST) {
            // Is it really necessary to use exceptions for flow control?
            $customer = CustomerFactory::createCustomer($converted_address[UnzerAddressConverter::FIRSTNAME], $converted_address[UnzerAddressConverter::LASTNAME])
              ->setCustomerId($unzer_customer_id);

            if (!empty($converted_address[UnzerAddressConverter::COMPANY])) {
              $customer->setCompany($converted_address[UnzerAddressConverter::COMPANY]);
            }
            $unzer->createCustomer($customer);
          }
          else {
            // Rethrow other API exceptions
            throw $e;
          }
        }
      }

      if (isset($billing_address_unzer) && $billing_address_unzer instanceof Address) {
        $customer->setBillingAddress($billing_address_unzer);
      }

      $email = $order_user->getEmail();

      if (!empty($email)) {
        $customer->setEmail($email);
      }

      // Sync any changes made above with Unzer server.
      $unzer->updateCustomer($customer);

      /** @var \Drupal\commerce_price\Price $total_price */
      $total_price = $order->getTotalPrice();
      $total_price_number = (float) $total_price->getNumber();
      $total_price_currency_code = $total_price->getCurrencyCode();

      $paypage = new Paypage($total_price_number, $total_price_currency_code, $form['#return_url']);

      $paypage->setCard3ds($gateway_settings['enable_3ds']);

      $excluded_types = array_keys(array_filter($gateway_settings['excluded_payment_types']));
      $optional_paypage_settings = $gateway_settings['paypage_settings'];

      // Allow custom code to modify paypage settings and payment type
      // exclusions.
      /** @var \Drupal\commerce_unzer\Event\PreOffsiteRedirectEvent $pre_offsite_redirect_event */
      $pre_offsite_redirect_event = $this->eventDispatcher->dispatch(new PreOffsiteRedirectEvent($order, $optional_paypage_settings, $excluded_types));
      $optional_paypage_settings = $pre_offsite_redirect_event->getPaypageSettings();
      $excluded_types = $pre_offsite_redirect_event->getExcludedPaymentTypes();

      $unzer_order_id = $order->id();
      /** @var \Drupal\commerce_unzer\Event\DetermineUnzerOrderIdEvent $determine_unzer_order_id_event */
      $determine_unzer_order_id_event = $this->eventDispatcher->dispatch(new DetermineUnzerOrderIdEvent($order, $unzer_order_id));
      $unzer_order_id = $determine_unzer_order_id_event->getUnzerOrderId();

      $paypage->setOrderId($unzer_order_id);
      $paypage->setExcludeTypes($excluded_types);

      // Handle optional settings.
      foreach ($payment_gateway_plugin->getUnzerOptionalPaypageSettings() as $setting_name => $setting_data) {
        // Set the value if provided by the user. Ignore any all-whitespace
        // values.
        if (isset($gateway_settings['paypage_settings'][$setting_name]) && !preg_match('@^\s*$@', (string) $gateway_settings['paypage_settings'][$setting_name])) {
          $method = $setting_data['method'];
          $paypage->$method($optional_paypage_settings[$setting_name]);
        }
      }

      // Collect basket info.
      $basket_items = [];

      // Order items.
      $order_items = $order->getItems();

      foreach ($order_items as $item) {
        if ($item->getUnitPrice()->isZero()) {
          // Unzer validation will reject prices of zero. Skip creating a basket item.
          continue;
        }
        $basket_item = new BasketItem();
        $basket_item->setTitle($item->getTitle())
          ->setQuantity((int) $item->getQuantity())
          ->setAmountPerUnitGross((float) $item->getUnitPrice()->getNumber());
        $basket_items[] = $basket_item;
      }

      // Item adjustments that are not included.
      $item_adjustments = [];

      foreach ($order->getItems() as $order_item) {
        $adjustments = array_filter($order_item->getAdjustments(), static fn ($adjustment) => !$adjustment->isIncluded());
        $item_adjustments[] = $adjustments;
      }
      $item_adjustments = array_merge(...$item_adjustments);
      // Order adjustments that are not included.
      $order_adjustments = array_filter($order->getAdjustments(), static fn ($adjustment) => !$adjustment->isIncluded());
      $all_adjustments = array_merge($item_adjustments, $order_adjustments);

      // The commerce coupon module splits up a fixed amount into separate adjustments per cart item.
      // In the Unzer basket we would like to have them consolidated into one single basket item.
      $consolidated_adjustment_data = array_reduce($all_adjustments, static function (array $carry, Adjustment $adjustment) {
        if ($adjustment->getAmount()->isZero()) {
          // Unzer validation will reject prices of zero. Skip creating a basket item.
          return $carry;
        }

        if ($adjustment->getType() !== 'promotion') {
          $adjustment_data = [
            'type' => $adjustment->getType(),
            'label' => $adjustment->getLabel(),
            'amount' => $adjustment->getAmount(),
          ];
          $carry['other_adjustments'][] = $adjustment_data;
        }
        else {
          if (!isset($carry['promotions'][$adjustment->getSourceId()])) {
            $adjustment_data = [
              'type' => $adjustment->getType(),
              'label' => $adjustment->getLabel(),
              'amount' => $adjustment->getAmount(),
            ];
            // Set initial values.
            $carry['promotions'][$adjustment->getSourceId()] = $adjustment_data;
          }
          else {
            // Add amount to existing ones.
            $carry['promotions'][$adjustment->getSourceId()]['amount'] = $carry['promotions'][$adjustment->getSourceId()]['amount']->add($adjustment->getAmount());
          }
        }

        return $carry;
      }, [
        'other_adjustments' => [],
        'promotions' => [],
      ]);

      $flattened_cad = array_merge(...array_values($consolidated_adjustment_data));

      // Create basked items for the consolidated adjustments.
      foreach ($flattened_cad as $item_data) {
        $basket_item = new BasketItem();
        $basket_item->setTitle($item_data['label'])
          ->setQuantity(1);

        // Set type and type-specific property.
        switch ($item_data['type']) {
          case 'promotion':
            $basket_item->setType(BasketItemTypes::VOUCHER);
            // Promotions in Drupal Commerce have a negative amount, discounts in the Unzer SDK have a positive amount.
            $basket_item->setAmountDiscountPerUnitGross(-(float) $item_data['amount']->getNumber());

            break;

          case 'shipping':
            $basket_item->setType(BasketItemTypes::SHIPMENT);
            $basket_item->setAmountPerUnitGross((float) $item_data['amount']->getNumber());

            break;

          default:
            $basket_item->setType(BasketItemTypes::GOODS);
            $basket_item->setAmountPerUnitGross((float) $item_data['amount']->getNumber());

            break;
        }

        $basket_items[] = $basket_item;
      }

      $basket = new Basket();
      $basket->setOrderId($order->id())
        ->setCurrencyCode($order->getTotalPrice()->getCurrencyCode())
        ->setTotalValueGross((float) $order->getTotalPrice()->getNumber())
        ->setBasketItems($basket_items);
      $unzer->initPayPageCharge($paypage, $customer, $basket);

      $order->setData('commerce_unzer', [
        'payment_id' => $paypage->getPaymentId(),
      ]);
      $order->save();
    }
    catch (\Exception $exception) {
      Error::logException($this->logger, $exception);
      $this->messenger->addError($this->t('An error has occurred trying to process the payment data, please contact us.'));

      $this->redirectToPaymentInformationPane($order);

      // We never get here due to a thrown NeedsRedirectException above.
      // This return is here to make the code checker happy.
      return [];
    }

    return $this->buildRedirectForm($form, $form_state, $paypage->getRedirectUrl(), [], self::REDIRECT_GET);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('messenger'),
      $container->get('datetime.time'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('commerce_unzer'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Redirects to the payment information pane on error.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @see \Drupal\commerce_payment\Plugin\Commerce\CheckoutPane\PaymentProcess::getErrorStepId()
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  protected function redirectToPaymentInformationPane(OrderInterface $order): void {
    try {
      /** @var \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesInterface $checkout_flow */
      // @phpstan-ignore-next-line
      $checkout_flow = $order->get('checkout_flow')->first()->get('entity')->getTarget()->getValue()->getPlugin();
      $step_id = $checkout_flow->getPane('payment_information')->getStepId();

      if ($step_id === '_disabled') {
        // Can't redirect to the _disabled step. This could mean that
        // isVisible() was overridden to allow PaymentProcess to be used without
        // a payment_information pane, but this method was not modified.
        throw new \RuntimeException('Cannot get the step ID for the payment_information pane. The pane is disabled.');
      }

      $checkout_flow->redirectToStep($step_id);
    }
    catch (\Exception) {
      $redirect_url = Url::fromRoute('<front>', ['absolute' => TRUE])->toString();

      throw new NeedsRedirectException($redirect_url);
    }
  }

}
